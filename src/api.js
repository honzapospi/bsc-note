import axios from "axios";

const rest = axios.create({
  baseURL: "http://private-9aad-note10.apiary-mock.com"
});

export const getNotes = () => {
  return rest.get("/notes");
};

export const deleteNote = id => {
  return rest.delete(`/notes/${id}`);
};

export const getNote = id => {
  return rest.get(`/notes/${id}`);
};

export const createNote = title => {
  return rest.post("/notes", { title });
};

export const updateNote = (id, title) => {
  return rest.put(`/notes/${id}`, { title });
};

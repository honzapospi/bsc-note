import { getNotes, getNote } from "./api";

describe("Testing api", () => {
  it("Test getNotes method.", () => {
    expect.assertions = 1;
    return getNotes().then(response => {
      expect(Array.isArray(response.data)).toBe(true);
    });
  });
  it("Test getNote method.", () => {
    expect.assertions = 1;
    return getNote(1).then(response => {
      expect(typeof response.data.id).toBe("number");
    });
  });
});

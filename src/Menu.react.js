import React from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

export default ({ handleChangeLang }) => {
  return (
    <div>
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <Link className="navbar-brand" to="/">
              NoteList
            </Link>
          </div>

          <div>
            <ul className="nav navbar-nav">
              <li>
                <Link to="/">
                  <FormattedMessage id="list" defaultMessage="List" />
                </Link>
              </li>
              <li>
                <Link to="/new">
                  <FormattedMessage id="new" defaultMessage="New" />
                </Link>
              </li>
            </ul>
          </div>
          <div style={{ float: "right" }}>
            <ul className="nav navbar-nav right">
              <li>
                <span onClick={() => handleChangeLang("cs")} className="link">
                  CS
                </span>
              </li>
              <li>
                <span onClick={() => handleChangeLang("en")} className="link">
                  EN
                </span>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

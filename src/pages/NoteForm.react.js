import React, { Component } from "react";
import { createNote, getNote, updateNote } from "../api";
import { FormattedMessage } from "react-intl";
const EDIT = "edit";
const CREATE = "create";

export default class extends Component {
  state = {
    title: ""
  };

  constructor(props) {
    super(props);
    const id = this.props.match.params.id;
    this.mode = id ? EDIT : CREATE;
  }

  handleChangeTitle = e => {
    this.setState({
      title: e.currentTarget.value
    });
  };

  handleFormSubmit = () => {
    if (this.state.title.trim() === "") {
      return alert("Please fill title.");
    }
    if (this.mode === CREATE) {
      createNote(this.state.title)
        .then(response => {
          console.log("New note has been created.");
          this.props.history.push("/");
        })
        .catch(e => {
          console.error(e);
        });
    } else if (this.mode === EDIT) {
      updateNote(this.props.match.params.id, this.state.title).then(
        response => {
          console.log("Note has been updated.");
          this.props.history.push("/");
        }
      );
    } else {
      console.log("Invalid mode");
    }
  };

  render() {
    const formTitle =
      this.mode === CREATE ? (
        <FormattedMessage id="new_note" defaultMessage="New note" />
      ) : (
        <FormattedMessage
          id="edit_note"
          defaultMessage="Edit note {id}"
          values={{ id: this.props.match.params.id }}
        />
      );

    return (
      <form style={{ margin: 50 }} onSubmit={e => e.preventDefault()}>
        <div>{formTitle}</div>
        <div className="form-group">
          <label htmlFor="title">
            <FormattedMessage id="title" defaultMessage="Title" />
          </label>
          <input
            type="text"
            className="form-control"
            id="title"
            value={this.state.title}
            onChange={this.handleChangeTitle}
          />
        </div>
        <button
          type="submit"
          className="btn btn-default"
          onClick={this.handleFormSubmit}
        >
          <FormattedMessage id="submit" defaultMessage="Submit" />
        </button>
      </form>
    );
  }

  componentDidMount() {
    if (this.mode === EDIT) {
      getNote(this.props.match.params.id).then(result => {
        this.setState({
          title: result.data.title
        });
      });
    }
  }
}

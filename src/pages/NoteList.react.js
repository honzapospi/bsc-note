import React, { Component } from "react";
import { getNotes, deleteNote } from "../api";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

export default class extends Component {
  state = {
    notes: null
  };

  render() {
    return (
      <div>
        <div
          className="bs-example"
          data-example-id="striped-table"
          style={{ margin: 5 }}
        >
          <table className="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>
                  <FormattedMessage id="title" defaultMessage="Title" />
                </th>
                <th>
                  <FormattedMessage id="action" defaultMessage="Action" />
                </th>
              </tr>
            </thead>
            <tbody>{this.renderRows()}</tbody>
          </table>
        </div>
      </div>
    );
  }
  renderRows = () => {
    if (this.state.notes === null) {
      return (
        <tr>
          <td colSpan="3">Loading data</td>
        </tr>
      );
    } else if (this.state.notes.length === 0) {
      return (
        <tr>
          <td colSpan="3">No data</td>
        </tr>
      );
    } else {
      return this.state.notes.map(note => {
        return (
          <tr key={note.id.toString()}>
            <td>{note.id}</td>
            <td>{note.title}</td>
            <td>
              <span
                style={{ cursor: "pointer", color: "blue" }}
                onClick={e => this.handleDeleteItem(note.id)}
              >
                Delete
              </span>{" "}
              <Link
                style={{ cursor: "pointer", color: "blue" }}
                to={`/edit/${note.id}`}
              >
                Edit
              </Link>
            </td>
          </tr>
        );
      });
    }
  };

  handleDeleteItem = id => {
    deleteNote(id)
      .then(response => {
        console.log(`Note with id ${id} has been deleted.`);
        this.fetchNotesFromServer();
      })
      .catch(e => {
        console.error(e);
      });
  };

  fetchNotesFromServer() {
    getNotes().then(response => {
      this.setState({
        notes: response.data
      });
    });
  }

  componentDidMount() {
    this.fetchNotesFromServer();
  }
}

import React, { Component } from "react";
import NoteList from "./pages/NoteList.react";
import NoteForm from "./pages/NoteForm.react";
import { Route } from "react-router-dom";
import Menu from "./Menu.react";
import { IntlProvider, addLocaleData } from "react-intl";
import cs from "./intl/cs.json";
import csLocaleData from "react-intl/locale-data/cs";

addLocaleData(csLocaleData);

const messages = {
  cs
};

class App extends Component {
  state = {
    lang: "en"
  };

  handleChangeLang = lang => {
    this.setState({
      lang
    });
  };

  render() {
    return (
      <IntlProvider
        locale={this.state.lang}
        messages={messages[this.state.lang]}
      >
        <React.Fragment>
          <header>
            <Menu
              lang={this.state.lang}
              handleChangeLang={this.handleChangeLang}
            />
          </header>
          <Route path="/" exact component={NoteList} />
          <Route path="/new" exact component={NoteForm} />
          <Route path="/edit/:id" exact component={NoteForm} />
        </React.Fragment>
      </IntlProvider>
    );
  }
}

export default App;

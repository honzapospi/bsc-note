This application is Note list with read, update ,delete and created methods. It is connected to rest API at http://private-9aad-note10.apiary-mock.com

## Initialization

```
yarn install
```

## Start development

```
yarn start
```

## Build production

```
yarn build
```
